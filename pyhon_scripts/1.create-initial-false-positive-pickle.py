#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 19:14:35 2020
Updated on Thur Apr 15 2021

@author: darryloswald, denzelbrown
"""


##############################################################################
# Section 1: Import packages and Oct 1, 2020 data
##############################################################################


import datetime as datetime
import pandas as pd

input_df = pd.read_pickle('./../clean_data/cleaned-10-01-2020.pkl')


##############################################################################
# Section 2: Create empty dataframes for exporting at the end of this script
##############################################################################


initial_date = datetime.datetime(2020, 10, 1)

pdz_log_df = pd.DataFrame()
hwm_log_df = pd.DataFrame()
pcw_log_df = pd.DataFrame()
fcw_log_df = pd.DataFrame()
ufcw_log_df = pd.DataFrame()


##############################################################################
# Section 3: Loop through all of the statuses and find statuses that fit a
# collision event
##############################################################################


# cycle through all of the buses
bus_list = sorted(list(set(input_df['Vehicle'])))

for bus in bus_list:

    mask = input_df['Vehicle'] == bus
    this_bus_df = input_df[mask]
    this_bus_df = this_bus_df.reset_index(drop=True)

    false_pdz_count = 0
    false_hwm_count = 0

    false_pcw_count = 0
    false_fcw_count = 0
    false_ufcw_count = 0


##############################################################################
# Section 4: For each bus, check the data around each event to deem it a true
# or false positive
##############################################################################


    # here, it is important to understand that this code will be counting the
    # number of times eah of 5 different alerts occur. They are:
        # PDZ - pedestrian danger zone
        # HWM - headway monitoring warning
        # PCW - pedestrian collision warning
        # FCW - forward collision warning
        # UFCW - urban forward collision warning
    # PDZ and HWM are not collision avoidance warnings, but danger alerts
    # PCW, FCW, and UFCW are considered collision avoidance warnings


    # for each bus's data, find false positives
    for x in range(len(this_bus_df)):
        this_status_str = str(this_bus_df['Status'][x])


        # check to see if the status is one of the alerts of interest for this
        # study
        if ('-PDZ-' in this_status_str) | \
            ('Headway Warning' in this_status_str):
                
                if input_df['Speed'][x] == 0:
                    
                    if '-PDZ' in this_status_str:
                        false_pdz_count += 1
                    elif 'Headway Warning' in this_status_str:
                        false_hwm_count += 1
                
        
        elif ('PCW' in this_status_str) | \
            ('Forward Collision Warning' in this_status_str) | \
            ('UFCW' in this_status_str):
                
            # create a dataframe that includes the 15 seconds before and after
            # the event of interest. But make sure that if the event happens in
            # the first or last 15 seconds of the day then you only take the time
            # that occured prior or after the event
            if (x < 15) & ((len(this_bus_df) - 15) < x):
                pass
            
            elif x < 15:
                event_df = this_bus_df[0:x+15]
                  
            elif (len(this_bus_df) - 15) < x:
                event_df = this_bus_df[x-15:]
            
            else:
                event_df = this_bus_df[x-15:x+15]
        
        
            # if the speed of the event is 0 mph, then we can automatically rule
            # the event out as a false positive. So, before performing the
            # accepted trajectory analysis we will add events with a speed of 0 to
            # our ongoing tally of false positives
            if input_df['Speed'][x] == 0:
                
                if 'PCW' in this_status_str:
                    false_pcw_count += 1
                elif 'Forward Collision Warning' in this_status_str:
                    false_fcw_count += 1    
                elif 'UFCW' in this_status_str:
                    false_ufcw_count += 1    
        
            # ruling out near miss collision events where the speed is 0mph,
            # the trajectory analysis will now be applied
            


    # add the count of each type of near miss to the respective dataframe
    pdz_log_df[bus] = [false_pdz_count]
    hwm_log_df[bus] = [false_hwm_count]
    pcw_log_df[bus] = [false_pcw_count]
    fcw_log_df[bus] = [false_fcw_count]
    ufcw_log_df[bus] = [false_ufcw_count]


##############################################################################
# Section 5: Add the date to each of the export dataframes
##############################################################################


pdz_log_df['Date'] = initial_date
hwm_log_df['Date'] = initial_date
pcw_log_df['Date'] = initial_date
fcw_log_df['Date'] = initial_date
ufcw_log_df['Date'] = initial_date


##############################################################################
# Section 6: Export the dataframes to pickle files
##############################################################################




