#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 28 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""

##############################################################################
# Section 1: Import packages and Oct 1, 2020 data
##############################################################################


import datetime
import pandas as pd

input_df = pd.read_pickle('./../clean_data/cleaned-10-1-2020.pkl')


##############################################################################
# Section 2: Create a dataframe that summarizes the near-miss alerts
##############################################################################


oct1 = datetime.datetime(2020, 10, 1)

pdz_df = pd.DataFrame()
hwm_df = pd.DataFrame()
pcw_df = pd.DataFrame()
fcw_df = pd.DataFrame()
ufcw_df = pd.DataFrame()

# loop through the input data and create a column for every bus
for bus in sorted(list(set(input_df['Vehicle']))):
    mask = input_df['Vehicle'] == bus
    this_bus_df = input_df[mask]
    this_bus_df = this_bus_df.reset_index(drop=True)
    
    # here, it is important to understand that this code will be counting the
    # number of times eah of 5 different alerts occur. They are:
        # PDZ - pedestrian danger zone
        # HWM - headway monitoring warning
        # PCW - pedestrian collision warning
        # FCW - forward collision warning
        # UFCW - urban forward collision warning
    # PDZ and HWM are not collision avoidance warnings, but danger alerts
    # PCW, FCW, and UFCW are considered collision avoidance warnings
    
    
    pdz_count = 0
    hwm_count = 0
    
    pcw_count = 0
    fcw_count = 0
    ufcw_count = 0
    
    for x in range(len(this_bus_df)):
        this_status_str = str(this_bus_df['Status'][x])
        
        
        # danger alerts
        
        # PDZ
        if '-PDZ-' in this_status_str:
            pdz_count += 1
        
        # HWM
        if 'Headway Warning' in this_status_str:
            hwm_count += 1
        
        
        # collision warnings
        
        
        # PCW
        if 'PCW' in this_status_str:
            pcw_count += 1
        
        # FCW
        if 'Forward Collision Warning' in this_status_str:
            fcw_count += 1
        
        # UFCW
        if 'UFCW' in this_status_str:
            ufcw_count += 1
        
    # now add the counts of each type to a df for each message type
    pdz_df[bus] = [pdz_count]
    hwm_df[bus] = [hwm_count]
    pcw_df[bus] = [pcw_count]
    fcw_df[bus] = [fcw_count]
    ufcw_df[bus] = [ufcw_count]
    
    
    
pdz_df['Date'] = oct1
hwm_df['Date'] = oct1
pcw_df['Date'] = oct1
fcw_df['Date'] = oct1
ufcw_df['Date'] = oct1


pdz_df = pdz_df.fillna(0.0)
hwm_df = hwm_df.fillna(0.0)
pcw_df = pcw_df.fillna(0.0)
fcw_df = fcw_df.fillna(0.0)
ufcw_df = ufcw_df.fillna(0.0)

# miles_df = miles_df.set_index('Date', drop=True)


##############################################################################
# Section 3: Export the pickle file
##############################################################################


pdz_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-pdz-log.pkl')
hwm_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-hwm-log.pkl')
pcw_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-pcw-log.pkl')
fcw_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-fcw-log.pkl')
ufcw_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-ufcw-log.pkl')

