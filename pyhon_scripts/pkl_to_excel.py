#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 26 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""

##############################################################################
# Converting Pickle files to Excel
##############################################################################
import pandas as pd
import os

# Insert neccessary files and paths

files = os.listdir("./../clean_data/")


path, dirs, files = next(os.walk("./../clean_data/csv/"))
file_count = len(files)

dataframes_list = []

for i in range(file_count):
    temp_df = pd.read_pickle("./../clean_data/csv"+files[i])
    dataframes_list.append(temp_df)
    
    

#Convert dataframe to excel

temp_df.to_excel("./../clean_data/clean_excel/")
