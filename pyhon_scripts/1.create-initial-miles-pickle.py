#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 25 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""

##############################################################################
# Section 1: Import packages and Oct 1, 2020 data
##############################################################################


import datetime
import pandas as pd

input_df = pd.read_pickle('./../clean_data/cleaned-10-1-2020.pkl')


##############################################################################
# Section 2: Create a dataframe that summarizes the distance traveled
##############################################################################


oct1 = datetime.datetime(2020, 10, 1)

miles_df = pd.DataFrame()

# loop through the input data and create a column for every bus
for bus in sorted(list(set(input_df['Vehicle']))):
    mask = input_df['Vehicle'] == bus
    this_bus_df = input_df[mask]
    max_miles = this_bus_df['Daily Miles'].max()
    miles_df[bus] = [max_miles]
    
miles_df['Date'] = oct1

miles_df = miles_df.fillna(0.0)

# miles_df = miles_df.set_index('Date', drop=True)


##############################################################################
# Section 3: Export the pickle file
##############################################################################


miles_df.to_pickle('./../log_pkl_files/0.fleet-miles-log.pkl')

