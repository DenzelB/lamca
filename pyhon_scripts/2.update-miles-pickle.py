#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 22 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""
##############################################################################
# Section 1: Import packages and set the initial input date
##############################################################################


from datetime import datetime
import math
import os
import pandas as pd


##############################################################################
# Section 2: Download pickle files in a loop
##############################################################################


# create a list of all of the pickle files that include data
files_list = os.listdir()

# create a list that only includes pickle files that store raw data
data_files = []
for file in files_list:
    if file[-4:] == '.pkl':
        str_size = len(file)
        pickle_prefix_string = file[:str_size-4]
        
        if pickle_prefix_string[0] == 'c':
            pickle_data_string = pickle_prefix_string[8:]
            data_files.append(pickle_data_string)

data_files = sorted(data_files)

for input_date in data_files:

    input_date_obj = datetime.strptime(input_date, '%m-%d-%Y')
    
    log_df = pd.read_pickle('./../log_pkl_files/0.fleet-miles-log.pkl')
    data_df_str = 'cleaned-' + input_date + '.pkl'
    data_df = pd.read_pickle(data_df_str)


##############################################################################
# Section 3: Determine which buses already exist in the daily miles log
##############################################################################


    existing_bus_list = []
    for column_header in list(log_df.columns):
        if type(column_header) == int:
            existing_bus_list.append(column_header)


##############################################################################
# Section 4: Loop through the daily cleaned data and add the miles traveled to
# the log df
##############################################################################


    # make sure that this is new data before doing any analysis
    if input_date_obj in list(log_df['Date']):
        pass
    else:
    
        data_df_bus_list = sorted(list(set(data_df['Vehicle'])))
        export_df = log_df.copy(deep=True)
        
        counter = 0
        for bus in data_df_bus_list:
            
            # now use a mask to see all of the odometer data for this particular bus
            mask = data_df['Vehicle'] == bus
            this_bus_df = data_df[mask]
            
            this_bus_daily_miles = this_bus_df['Daily Miles']
            
            # take the maximum value from the daily miles series
            max_daily_miles = round(this_bus_daily_miles.max(),1)
            
            if math.isnan(max_daily_miles):
                max_daily_miles = 0.0
            
            # check to see if this bus has been recorded before, and if so add the
            # data to the dataframe
            if bus in existing_bus_list:
                data = {bus:max_daily_miles} 
                
                # use a counter to limit the appending of a row so that it happens once
                if counter == 0:
                    initial_len_export_df = len(export_df)
                    export_df = export_df.append(pd.DataFrame(data, index=[initial_len_export_df]))
                    export_df.loc[initial_len_export_df, ('Date')] = input_date_obj
                    counter = counter + 1
                
                # after appending, place the miles totals in an exact location
                else:
                    export_df.loc[initial_len_export_df, bus] = max_daily_miles
            
            # since this bus was just added to the fleet and has not been in the log
            # before, 
            else:
                data = {bus:max_daily_miles}
                export_df[bus] = data
                export_df = export_df.fillna(0.0)

        export_df = export_df.sort_values(by="Date")
        export_df = export_df.fillna(0.0)


##############################################################################
# Section 5: Export the updated log to a pickle file
##############################################################################
    
    
        export_df.to_pickle('./../log_pkl_files/0.fleet-miles-log.pkl')
    
        
        
        