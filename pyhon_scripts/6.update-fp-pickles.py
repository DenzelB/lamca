#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mar 11 2021
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""


"""
The purpose of this script is to create the initial false positive pickle file
for each event type. This will be used one time to set up the initial pickle
file, and then every other time that this analysis occurs, another script will
be used to update the product of this script.
"""

##############################################################################
# Section 1: Import necessary packages and data
##############################################################################


import os
import pandas as pd
from datetime import datetime


# list all of the event types
event_type_list = [
    'pcw',
    'pdz',
    'fcw',
    'ufcw',
    'hwm'
    ]


##############################################################################
# Section 2: Loop through all of the false positive keys and create a
# dataframe that lists all of the false positive counts
##############################################################################


# now loop through every event type and create a false positive pickle for
# that event
for event_str in event_type_list:
    
    # load the fp event dataframe and maks out any rejected false positives
    event_fp_pickle_str = event_str + '-false-positives.pkl'
    curr_fp_df = pd.read_pickle(event_fp_pickle_str)
    
    mask = curr_fp_df['False Positive']
    true_curr_fp_df = curr_fp_df[mask]
    
    # load the initial near miss pickle
    path = os.getcwd()
    path2 = os.path.dirname(path)
    os.chdir(path2)
    
    event_near_miss_pickle_str = '0.fleet-near-miss-' + event_str + '-log.pkl'
    initial_log = pd.read_pickle(event_near_miss_pickle_str)


    # replace all of the values within the initial log equal to 0
    modified_log = initial_log.copy(deep=True)
    
    # loop through all of the columns that are not the date column replace the
    # value with 0
    for column_name in modified_log.columns:
        if column_name != 'Date':
            modified_log[column_name] = 0
            
    # add a date object column to the modified log
    date_obj_list = []
    for date in modified_log['Date']:
        date_obj = date.date()
        date_obj_list.append(date_obj)
    modified_log['Date Object'] = date_obj_list


    for vehicle in sorted(list(set(true_curr_fp_df['Vehicle']))):
        
        # isolate the confirmed false positives
        mask = true_curr_fp_df['Vehicle'] == vehicle
        this_bus_df = true_curr_fp_df[mask]
        this_bus_df = this_bus_df.reset_index(drop=True)
        
              
        # for each row in the this_bus_df, add the 
        for x in range(len(this_bus_df)):
            curr_date = this_bus_df['Datetime'][x].date()
            
            
            if curr_date in list(modified_log['Date Object']):
                
                # determine the index of the date
                index = list(modified_log['Date Object']).index(curr_date)
                
                
                # the date of this row of the false positive dataframe is in
                # the near-miss dataframe for this event type
                modified_log[vehicle][index] += 1
    
    
    # change the directory back
    os.chdir(path)
    
    
    #export the modified log
    export_file_str = '0.' + event_str + '-false-positive-log.pkl'
    modified_log.to_pickle(export_file_str)
        

