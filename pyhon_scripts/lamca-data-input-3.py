#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 11 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""

##############################################################################
# Section 1: Import packages and set input variables
##############################################################################


import pandas as pd
import datetime
import os


save_pickle_path = "./../raw_data"


##############################################################################
# Section 2: Download the excel report that was sent over email
##############################################################################


cwd = os.listdir()

excel_file_list = []
for file in cwd:
    if file[-5:] == '.xlsx':
        excel_file_list.append(file)


##############################################################################
# Section 3: Save the file as a pickle file
##############################################################################


for file in excel_file_list:
    excel_file_name = file
    
    df = pd.read_excel(file)
    
    # reorganize the dataframe to be more compatible with future python use
    df = df.iloc[6:]
    df.columns = df.iloc[0]
    df = df.reset_index(drop=True)
    df = df.iloc[1:]
    df = df.reset_index(drop=True)
    
    # find the datetime associated with this dataframe
    date_str_import = df['Loc Time'][100]
    date_obj = datetime.datetime.strptime(date_str_import, '%m/%d/%Y %H:%M:%S')
    
    # create a name for the export pkl file based on the date
    day = date_obj.day
    month = date_obj.month
    year = date_obj.year
    date_str_export = save_pickle_path + str(month) + '-' + str(day) + '-' + str(year) + '.pkl'
    
    # save the dataframe as a pkl file
    df.to_pickle(date_str_export)
    
    
##############################################################################
# Section 4: Delete the downloaded excel file from the directory
##############################################################################
    
    
    os.remove(excel_file_name)

