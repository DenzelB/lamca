#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mar 10 2021
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""


"""
The purpose of this script is to chart the speed profiles that have been
approved/accepted after running the false positive analysis. This script is
originally intended to create a chart for only one near miss alert/warning
at a time.
"""

##############################################################################
# Section 1: Import necessary packages and data
##############################################################################


import os
import pandas as pd
import plotly.graph_objects as go


input_speed_profiles = pd.read_pickle()


##############################################################################
# Section 2: Chart the percentage-based speed profiles.
##############################################################################


# graph the percentage thing
fig = go.Figure(data=go.Scatter(x = input_speed_profiles.columns,
                                y = list(input_speed_profiles.iloc[0]),
                                mode = 'lines+markers',
                                marker_color = 'black'))


for x in range(len(input_speed_profiles)):
    if x == 0:
        pass
    else:
        fig.add_trace(go.Scatter(x = input_speed_profiles.columns,
                                        y = list(input_speed_profiles.iloc[x]),
                                        mode = 'lines+markers',
                                        marker_color = 'black'))
        
        
fig.update_traces(opacity=0.05, selector=dict(type='scatter'))
fig.update_layout(title = 'Speed profiles of pcw events as percentage of speed at time of events.',
                  xaxis_title = 'Time in seconds relative to time of event (where time t = 0 is the time of event)',
                  yaxis_title = 'Speed of vehicle as a percentage of speed at time of event')


fig.write_html('pcw-percent.html')



