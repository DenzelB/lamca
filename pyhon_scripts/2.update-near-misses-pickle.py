#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 25 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""

##############################################################################
# Section 1: Import packages
##############################################################################


from datetime import datetime
import os
import pandas as pd


##############################################################################
# Section 2: Download pickle files in a loop
##############################################################################


# create a list of all of the pickle files that include data
files_list = os.listdir()

# create a list that only includes pickle files that store raw data
data_files = []
for file in files_list:
    if file[-4:] == '.pkl':
        str_size = len(file)
        pickle_prefix_string = file[:str_size-4]
        
        if pickle_prefix_string[0] == 'c':
            pickle_data_string = pickle_prefix_string[8:]
            data_files.append(pickle_data_string)

data_files = sorted(data_files)

for input_date in data_files:

    input_date_obj = datetime.strptime(input_date, '%m-%d-%Y')
    
    pdz_log_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-pdz-log.pkl')
    hwm_log_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-hwm-log.pkl')
    pcw_log_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-pcw-log.pkl')
    fcw_log_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-fcw-log.pkl')
    ufcw_log_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-ufcw-log.pkl')
    
    data_df_str = 'cleaned-' + input_date + '.pkl'
    data_df = pd.read_pickle(data_df_str)
    
    append_counter = 0


##############################################################################
# Section 3: Determine which buses already exist in the daily miles log
##############################################################################


    existing_bus_list = []
    for column_header in list(pdz_log_df.columns):
        if type(column_header) == int:
            existing_bus_list.append(column_header)


##############################################################################
# Section 4: Create a dataframe that summarizes the near-miss alerts
##############################################################################


    # create export dataframes, that will be a version of the input log
    # dataframes for every near-miss type, but these will have a row appended
    # to them to include data from whatever data is currently being analyzed
    # in the loop
    pdz_export_df = pdz_log_df.copy(deep=True)
    hwm_export_df = hwm_log_df.copy(deep=True)
    pcw_export_df = pcw_log_df.copy(deep=True)
    fcw_export_df = fcw_log_df.copy(deep=True)
    ufcw_export_df = ufcw_log_df.copy(deep=True)


    # make sure that this is new data before doing any analysis
    if input_date_obj in list(pdz_log_df['Date']):
        pass
    else:
        
        # loop through the input data and create a column for every bus
        for bus in sorted(list(set(data_df['Vehicle']))):
            mask = data_df['Vehicle'] == bus
            this_bus_df = data_df[mask]
            this_bus_df = this_bus_df.reset_index(drop=True)
            
            # here, it is important to understand that this code will be counting the
            # number of times eah of 5 different alerts occur. They are:
                # PDZ - pedestrian danger zone
                # HWM - headway monitoring warning
                # PCW - pedestrian collision warning
                # FCW - forward collision warning
                # UFCW - urban forward collision warning
            # PDZ and HWM are not collision avoidance warnings, but danger alerts
            # PCW, FCW, and UFCW are considered collision avoidance warnings
            
            
            pdz_count = 0
            hwm_count = 0
            
            pcw_count = 0
            fcw_count = 0
            ufcw_count = 0
            
            for x in range(len(this_bus_df)):
                this_status_str = str(this_bus_df['Status'][x])
                
                
                # danger alerts
                
                # PDZ
                if '-PDZ-' in this_status_str:
                    pdz_count += 1
                
                # HWM
                if 'Headway Warning' in this_status_str:
                    hwm_count += 1
                
                
                # collision warnings
                
                
                # PCW
                if 'PCW' in this_status_str:
                    pcw_count += 1
                
                # FCW
                if 'Forward Collision Warning' in this_status_str:
                    fcw_count += 1
                
                # UFCW
                if 'UFCW' in this_status_str:
                    ufcw_count += 1
            
            pdz_data = {bus:pdz_count}
            hwm_data = {bus:hwm_count}
            pcw_data = {bus:pcw_count}
            fcw_data = {bus:fcw_count}
            ufcw_data = {bus:ufcw_count}
            
            # now add the counts of each type to a df for each message type
            # but first add a row to the log dataframe
            
            if append_counter == 0:
                initial_len_export_df = len(pdz_log_df)
                
                pdz_export_df = pdz_export_df.append(pd.DataFrame(pdz_data, index=[initial_len_export_df]))            
                hwm_export_df = hwm_export_df.append(pd.DataFrame(hwm_data, index=[initial_len_export_df]))
                pcw_export_df = pcw_export_df.append(pd.DataFrame(pcw_data, index=[initial_len_export_df]))
                fcw_export_df = fcw_export_df.append(pd.DataFrame(fcw_data, index=[initial_len_export_df]))
                ufcw_export_df = ufcw_export_df.append(pd.DataFrame(ufcw_data, index=[initial_len_export_df]))
                
                pdz_export_df.loc[initial_len_export_df, ('Date')] = input_date_obj
                hwm_export_df.loc[initial_len_export_df, ('Date')] = input_date_obj
                pcw_export_df.loc[initial_len_export_df, ('Date')] = input_date_obj
                fcw_export_df.loc[initial_len_export_df, ('Date')] = input_date_obj
                ufcw_export_df.loc[initial_len_export_df, ('Date')] = input_date_obj
                
                append_counter += 1
                
            else:
                
                pdz_export_df.loc[initial_len_export_df, bus] = pdz_count
                hwm_export_df.loc[initial_len_export_df, bus] = hwm_count
                pcw_export_df.loc[initial_len_export_df, bus] = pcw_count
                fcw_export_df.loc[initial_len_export_df, bus] = fcw_count
                ufcw_export_df.loc[initial_len_export_df, bus] = ufcw_count
            
        
    pdz_export_df = pdz_export_df.fillna(0.0)
    hwm_export_df = hwm_export_df.fillna(0.0)
    pcw_export_df = pcw_export_df.fillna(0.0)
    fcw_export_df = fcw_export_df.fillna(0.0)
    ufcw_export_df = ufcw_export_df.fillna(0.0)


##############################################################################
# Section 5: Export the pickle file
##############################################################################


    pdz_export_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-pdz-log.pkl')
    hwm_export_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-hwm-log.pkl')
    pcw_export_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-pcw-log.pkl')
    fcw_export_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-fcw-log.pkl')
    ufcw_export_df.to_pickle('./../log_pkl_files/0.fleet-near-miss-ufcw-log.pkl')

