#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Dec 21 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""

##############################################################################
# Section 1: Import necessary packages and files
##############################################################################


import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots


# open all of the 12/17 data as a pkl file
miles_df = pd.read_pickle('./../log_pkl_files/0.fleet-miles-log.pkl')
fcw_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-fcw-log.pkl')
hwm_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-hwm-log.pkl')
pcw_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-pcw-log.pkl')
pdz_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-pdz-log.pkl')
ufcw_df = pd.read_pickle('./../log_pkl_files/0.fleet-near-miss-ufcw-log.pkl')


##############################################################################
# Section 2: Find the total amount of miles and near misses
##############################################################################


# assign names to each df
miles_df.name = 'miles'
fcw_df.name = 'fcw'
hwm_df.name = 'hwm'
pcw_df.name = 'pcw'
pdz_df.name = 'pdz'
ufcw_df.name = 'ufcw'


df_names_list = [miles_df, fcw_df, hwm_df, pcw_df, pdz_df, ufcw_df]


# find the total amount of miles, near misses
totals_dict = dict()

for x in range(len(df_names_list)):
    df = df_names_list[x].copy(deep=True)
    
    del df['Date']
    
    total_series = df.sum()
    total = int(sum(total_series))
    metric = df_names_list[x].name
    
    totals_dict[metric] = total
    

# sort all of the dataframes by date, so that 
miles_df = miles_df.sort_values(by=['Date'])
fcw_df = fcw_df.sort_values(by=['Date'])
hwm_df = hwm_df.sort_values(by=['Date'])
pcw_df = pcw_df.sort_values(by=['Date'])
pdz_df = pdz_df.sort_values(by=['Date'])
ufcw_df = ufcw_df.sort_values(by=['Date'])


# remove the october 1st date from all of the dataframes. this will be done
miles_df_no_oct1 = miles_df.iloc[1:]
fcw_df_no_oct1 = fcw_df.iloc[1:]
hwm_df_no_oct1 = hwm_df.iloc[1:]
pcw_df_no_oct1 = pcw_df.iloc[1:]
pdz_df_no_oct1 = pdz_df.iloc[1:]
ufcw_df_no_oct1 = ufcw_df.iloc[1:]


# reindex all of the dataframes
miles_df_no_oct1 = miles_df_no_oct1.reset_index(drop=True)
fcw_df_no_oct1 = fcw_df_no_oct1.reset_index(drop=True)
hwm_df_no_oct1 = hwm_df_no_oct1.reset_index(drop=True)
pcw_df_no_oct1 = pcw_df_no_oct1.reset_index(drop=True)
pdz_df_no_oct1 = pdz_df_no_oct1.reset_index(drop=True)
ufcw_df_no_oct1 = ufcw_df_no_oct1.reset_index(drop=True)

#   # export the fcw df no oct 1 to excel
#   fcw_df_no_oct1.to_excel('fcw_pre-miles-filter.xlsx')

no_oct1_alert_list = [fcw_df_no_oct1, hwm_df_no_oct1, pcw_df_no_oct1,
                      pdz_df_no_oct1, ufcw_df_no_oct1]


##############################################################################
# Section 3: Make sure that no events are counted on days when the miles
# traveled are less than 1
##############################################################################


# loop through all of the _no_oct1 dataframes for each alert type and check
# the corresponding miles count. If it is less than 1, overwrite the number
# of alerts to 0
miles_df_no_oct1_row_names = list(miles_df_no_oct1.columns)
filter_export_dict = {
    'Bus': [],
    'Datetime': [],
    'Miles': []
    }


for no_oct1_df in no_oct1_alert_list:
    rows = no_oct1_df.shape[0]
    column_names = list(no_oct1_df.columns)
    
    # cycle through each cardinal position, 
    for column in column_names:
        
        if column != 'Date':
        
            for row in range(rows):
            
                alert_data = no_oct1_df[column][row]
                miles_data = miles_df_no_oct1[column][row]
            
                if miles_data < 1:
                    new_alert_data = 0
                    
                    no_oct1_df[column][row] = new_alert_data
                    
                    filter_export_dict['Bus'].append(column)
                    filter_export_dict['Datetime'].append(no_oct1_df['Date'][row])
                    filter_export_dict['Miles'].append(miles_data)


# export the filtered export dictionary as a pickle file
filtered_data_key = pd.DataFrame(filter_export_dict)
filtered_data_key.to_pickle('filtered-miles-data-key.pkl')
            

##############################################################################
# Section 4: Export the cleaned dataframes to excel so that they can be
# included in the report's appendix section
##############################################################################


# now export the no oct 1 dataframes to excel so that they can be included in
# the report
# miles_df_no_oct1.to_excel('Interim Report Miles Log.xlsx')
# fcw_df_no_oct1.to_excel('Interim Report FCW Log.xlsx')
# hwm_df_no_oct1.to_excel('Interim Report HWM Log.xlsx')
# pcw_df_no_oct1.to_excel('Interim Report PCW Log.xlsx')
# pdz_df_no_oct1.to_excel('Interim Report PDZ Log.xlsx')
# ufcw_df_no_oct1.to_excel('Interim Report UFCW Log.xlsx')


##############################################################################
# Section 5: Find the amount of near misses per 1,000 for each of the
# near-miss parameters
##############################################################################


# find the amount of miles per 7 days by resampling
resamp_miles = miles_df_no_oct1.resample('7D', on='Date').sum()
resamp_miles['Total'] = resamp_miles.sum(axis=1)

resamp_fcw = fcw_df_no_oct1.resample('7D', on='Date').sum()
resamp_fcw['Total'] = resamp_fcw.sum(axis=1)

resamp_hwm = hwm_df_no_oct1.resample('7D', on='Date').sum()
resamp_hwm['Total'] = resamp_hwm.sum(axis=1)

resamp_pcw = pcw_df_no_oct1.resample('7D', on='Date').sum()
resamp_pcw['Total'] = resamp_pcw.sum(axis=1)

resamp_pdz = pdz_df_no_oct1.resample('7D', on='Date').sum()
resamp_pdz['Total'] = resamp_pdz.sum(axis=1)

resamp_ufcw = ufcw_df_no_oct1.resample('7D', on='Date').sum()
resamp_ufcw['Total'] = resamp_ufcw.sum(axis=1)


# create variables for the total columns of each metric
total_miles = resamp_miles['Total']
total_fcw = resamp_fcw['Total']
total_hwm = resamp_hwm['Total']
total_pcw = resamp_pcw['Total']
total_pdz = resamp_pdz['Total']
total_ufcw = resamp_ufcw['Total']


# find the number of near misses per 1,000 miles
fcw_rate = round((total_fcw / total_miles) * 1000, 0)
hwm_rate = round((total_hwm / total_miles) * 1000, 0)
pcw_rate = round((total_pcw / total_miles) * 1000, 0)
pdz_rate = round((total_pdz / total_miles) * 1000, 0)
ufcw_rate = round((total_ufcw / total_miles) * 1000, 0)


# find the total near misses per 1,000 miles each week
total_near_miss_rate = fcw_rate + hwm_rate + pcw_rate + pdz_rate + ufcw_rate


# find the total near misses when broken down into driver behavior alerts
# (pdz, hwm) and collision warnings (fcw, pcw, ufcw)
driver_behavior_alerts_rate = pdz_rate + hwm_rate
collision_warnings_rate = fcw_rate + pcw_rate + ufcw_rate


# find each of the rates, and the rate categorizations as a percentage of the
# total
fcw_rate_percentage = round((fcw_rate / total_near_miss_rate) * 100, 2)
hwm_rate_percentage = round((hwm_rate / total_near_miss_rate) * 100, 2)
pcw_rate_percentage = round((pcw_rate / total_near_miss_rate) * 100, 2)
pdz_rate_percentage = round((pdz_rate / total_near_miss_rate) * 100, 2)
ufcw_rate_percentage = round((ufcw_rate / total_near_miss_rate) * 100, 2)

driver_behavior_alerts_rate_percentage = round((driver_behavior_alerts_rate / total_near_miss_rate) * 100, 2)
collision_warnings_rate_percentage = round((collision_warnings_rate / total_near_miss_rate) * 100, 2)


# combine all of the rates and percentages into dfs and export to excel for
# reference
rate_df = pd.DataFrame({"FCW Rate": fcw_rate,
                        "HWM Rate": hwm_rate,
                        "PCW Rate": pcw_rate,
                        "PDZ Rate": pdz_rate,
                        "UFCW Rate": ufcw_rate, 
                        "Driver Behavior Alert Rate": driver_behavior_alerts_rate,
                        "Collision Warning Rate": collision_warnings_rate,
                        "Total Near Misses Rate": total_near_miss_rate})


rate_percentage_df = pd.DataFrame({"FCW Rate (%)": fcw_rate_percentage,
                        "HWM Rate (%)": hwm_rate_percentage,
                        "PCW Rate (%)": pcw_rate_percentage,
                        "PDZ Rate (%)": pdz_rate_percentage,
                        "UFCW Rate (%)": ufcw_rate_percentage, 
                        "Driver Behavior Alert Rate (%)": driver_behavior_alerts_rate_percentage,
                        "Collision Warning Rate (%)": collision_warnings_rate_percentage})


rate_df.to_excel('Near Misses per 1,000 Miles.xlsx')
rate_percentage_df.to_excel('Near Misses per 1,000 Miles as Percentage.xlsx')


##############################################################################
# Section 6: Make pretty graphs out of all of the data
##############################################################################


# make a date series without the time for the following graph x axis
date_series = pd.to_datetime(fcw_rate.index).date


# rate line graph figure
rate_fig = go.Figure()
rate_fig = make_subplots(specs=[[{"secondary_y": True}]])
rate_fig.add_trace(go.Scatter(x=date_series, y=fcw_rate,
                    mode='lines+markers',
                    name='FCW',
                    line=dict(width=5),
                    marker=dict(size=10, line=dict(width=1.5, color='DarkSlateGrey'))),
                   secondary_y=False)
rate_fig.add_trace(go.Scatter(x=date_series, y=hwm_rate,
                    mode='lines+markers',
                    name='HWM',
                    line=dict(width=5),
                    marker=dict(size=10, line=dict(width=1.5, color='DarkSlateGrey'))),
                   secondary_y=False)
rate_fig.add_trace(go.Scatter(x=date_series, y=pcw_rate,
                    mode='lines+markers',
                    name='PCW',
                    line=dict(width=5),
                    marker=dict(size=10, line=dict(width=1.5, color='DarkSlateGrey'))),
                   secondary_y=False)
rate_fig.add_trace(go.Scatter(x=date_series, y=pdz_rate,
                    mode='lines+markers',
                    name='PDZ',
                    line=dict(width=5),
                    marker=dict(size=10, line=dict(width=1.5, color='DarkSlateGrey'))),
                   secondary_y=False)
rate_fig.add_trace(go.Scatter(x=date_series, y=ufcw_rate,
                    mode='lines+markers',
                    name='UFCW',
                    line=dict(width=5),
                    marker=dict(size=10, line=dict(width=1.5, color='DarkSlateGrey'))),
                   secondary_y=False)
# rate_fig.add_trace(go.Scatter(x=date_series, y=total_miles,
#                     mode='lines+markers',
#                     name='Fleet Miles Driven',
#                     line=dict(width=5, dash='dash'),
#                     marker=dict(size=10, line=dict(width=1.5, color='DarkSlateGrey'))),
#                    secondary_y=True)
rate_fig.update_layout(template='none', yaxis_title='Alerts per 1,000 miles')
rate_fig.update_xaxes(tickmode='array', tickvals=date_series, ticktext=date_series)
# rate_fig.update_yaxes(title_text="Fleet Miles Driven", secondary_y=True)
rate_fig.update_yaxes(separatethousands=True)
rate_fig.write_html('0.rate.html')


# percentage fo rate bar graph figure
percentage_fig = go.Figure(data=[
    go.Bar(name='FCW Rate (<1%)', x=date_series, y=fcw_rate_percentage,
           text=fcw_rate_percentage, textposition='none'),
    go.Bar(name='HWM Rate', x=date_series, y=hwm_rate_percentage,
           text=hwm_rate_percentage, textposition='inside'),
    go.Bar(name='PCW Rate', x=date_series, y=pcw_rate_percentage,
           text=pcw_rate_percentage, textposition='inside'),
    go.Bar(name='PDZ Rate', x=date_series, y=pdz_rate_percentage,
           text=pdz_rate_percentage, textposition='inside'),
    go.Bar(name='UFCW Rate', x=date_series, y=ufcw_rate_percentage,
           text=ufcw_rate_percentage, textposition='inside')
])
# Change the bar mode
percentage_fig.update_layout(barmode='stack', template='none')
percentage_fig.update_xaxes(tickmode='array', tickvals=date_series, ticktext=date_series)
percentage_fig.update_yaxes(ticksuffix='%')
percentage_fig.write_html('0.percentage.html')


# percentage for rate bar graph figure
percentage_summary_fig = go.Figure(data=[
    go.Bar(name='Driver Behavior Alerts Rate', x=date_series, y=driver_behavior_alerts_rate_percentage,
           text=driver_behavior_alerts_rate_percentage, textposition='inside'),
    go.Bar(name='Collision Warnings Rate', x=date_series, y=collision_warnings_rate_percentage,
           text=collision_warnings_rate_percentage, textposition='inside')
])
# Change the bar mode
percentage_summary_fig.update_layout(barmode='stack', template='none')
percentage_summary_fig.update_xaxes(tickmode='array', tickvals=date_series, ticktext=date_series)
percentage_summary_fig.update_yaxes(ticksuffix='%')
percentage_summary_fig.write_html('0.percentage summary.html')






