#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Feb 27 2021
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""


"""
The purpose of this program is to develop a dataframe of speed profiles that
correspond to the 10 seconds before and after a pcw event.

These profiles will be used later to determine which pcw events can be
considered false positives.
"""

##############################################################################
# Section 1: Import packages and necessary data
##############################################################################


import pandas as pd
import os
import numpy as np
from datetime import datetime


# now cycle through all of the cleaned data pickle files for the false
# positive analysis
path = os.getcwd()
path2 = os.path.dirname(path)

# load the list dataframe that outlines every day that a vehicle drove for
# less than 1 mile so that speed profiles that occur on these days can be
# removed from the analysis
input_filtered_miles_key = pd.read_pickle('filtered-miles-data-key.pkl')


os.chdir(path2)


##############################################################################
# Section 2: Create a list of all of the cleaned data file names
##############################################################################


# these file names will be used in a loop in a later section
files = os.listdir()
cleaned_data_file_names = []
for x in range(len(files)):

    curr_file = files[x]

    if curr_file[0] == 'c':
        
        # the file is a "cleaned-date.pkl" file
        cleaned_data_file_names.append(curr_file)
        

##############################################################################
# Section 3: Add a date column to the input filtered miles key dataframe
##############################################################################


date_list = []

for x in range(len(input_filtered_miles_key)):
    
    curr_date = input_filtered_miles_key['Datetime'][x].date()
    date_list.append(curr_date)
    

input_filtered_miles_key['Date'] = date_list


##############################################################################
# Section 4: With the cleaned data, figure out what the speed profiles are for
# each event. Create a dataframe to hol the profiles.
##############################################################################


speed_profile_rows =[]
datetime_obj_list = []
vehicle_list = []


# loop through each cleaned data file
for file in cleaned_data_file_names:
    
    curr_file_df = pd.read_pickle(file)
    
    
    # loop through the rows of the cleaned dataframe
    for x in range(len(curr_file_df)):
        
        this_status_str = str(curr_file_df['Status'][x])
        
        if 'PCW' in this_status_str:
        
            occurs_on_filter_day = False
            
            this_bus = curr_file_df['Vehicle'][x]
            this_date_str = curr_file_df['Datetime'][x]
            this_date_obj = datetime.strptime(this_date_str, '%m/%d/%Y %H:%M:%S').date()
            
            
            # check to see if the vehicle on this day drove for less than 1 mile.
            # If so, do not include data from this day in the analysis
            
            
            this_vehicle_mask = input_filtered_miles_key['Bus'] == this_bus
            this_vehicle_miles_filter_df = input_filtered_miles_key[this_vehicle_mask].reset_index(drop=True)
            
            if len(this_vehicle_miles_filter_df) > 0:
                
                # this bus did have at least one day where it drove for less than
                # one mile
                
                # loop through the filter dataframe to see if any of the listed
                # days where the bus drove for less than one mile
                for y in range(len(this_vehicle_miles_filter_df)):
                    curr_filter_date = this_vehicle_miles_filter_df['Date'][y]
                    
                    if this_date_obj == curr_filter_date:
                        occurs_on_filter_day = True
            
            
            # now check to see if the current row of data has a pcw status code.
            # If so, add the speed data to the speed profile dataframe and add the
            # datetime to the datetime dataframe
            if occurs_on_filter_day == False:
                
                # add the 10 seconds before and after an alert to the pcw_df
                speed_data_to_be_added = list(curr_file_df['Speed'][x-10:x+11])
                speed_profile_rows.append(speed_data_to_be_added)  
                
                
                # add the datetime to another list to merge the false positve and
                # negative analysis with these speed profiles
                curr_datetime_str = curr_file_df['Datetime'][x]
                curr_datetime_obj = datetime.strptime(curr_datetime_str,
                                                      '%m/%d/%Y %H:%M:%S')
                datetime_obj_list.append(curr_datetime_obj)
                
                
                # also add the vehicle name to the vehicle list
                vehicle_list.append(curr_file_df['Vehicle'][x])


pcw_speed_profile = pd.DataFrame(speed_profile_rows,
                                  columns = [-10, -9, -8, -7, -6, -5, -4, -3,
                                            -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8,
                                            9, 10])


##############################################################################
# Section 5: Create a dataframe for the speed profile as a percentage of the
# speed at time t = 0.
##############################################################################


# for every row in the pcw speed profile dataframe, we need to convert the
# speed into the speed at that time divided by the speed of time t=0. Convert
# this decimal into a percent. Graph the percentage.
pcw_percentage_speed_rows = []
pcw_event_no_nan_datetimes_list = []
pcw_vehicle_no_nan_list = []

for x in range(len(pcw_speed_profile)):
    
    curr_row = pcw_speed_profile.iloc[x]
    curr_datetime = datetime_obj_list[x]
    curr_vehicle = vehicle_list[x]
    
    time0_speed = curr_row[0]
    
    export_row = []
    
    nan_status = False
    
    for y in range(len(pcw_speed_profile.columns)):
        
        curr_speed = curr_row[y-10]
        
        proportion_speed = curr_speed / time0_speed
        percent_speed = round(proportion_speed * 100, 0)
        
        export_row.append(percent_speed)
        
    # check to see if there are any nan values, and if so remove the profile
    # from the analysis
    for y in range(len(export_row)):
        curr_value = export_row[y]
        curr_nan_status = np.isnan(curr_value)
        
        if curr_nan_status:
            nan_status = True

    if nan_status == False:
        pcw_percentage_speed_rows.append(export_row)
        pcw_event_no_nan_datetimes_list.append(curr_datetime)
        pcw_vehicle_no_nan_list.append(curr_vehicle)
        

pcw_speed_profile_percentage = pd.DataFrame(pcw_percentage_speed_rows,
                         columns = [-10, -9, -8, -7, -6, -5, -4, -3,
                                    -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8,
                                    9, 10])

pcw_datetime_dict = {
    'Datetime': pcw_event_no_nan_datetimes_list,
    'Vehicle': pcw_vehicle_no_nan_list
    }
pcw_datetimes = pd.DataFrame(pcw_datetime_dict)


##############################################################################
# Section 6: Export the speed profile and datetimes to a pickle file.
##############################################################################


os.chdir(path)

pcw_speed_profile_percentage.to_pickle('pcw-speed-profile-percentage.pkl')
pcw_datetimes.to_pickle('pcw-event-datetimes.pkl')



