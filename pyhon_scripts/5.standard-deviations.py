#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mar 5 2021
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""


"""
This script is intended to determine the average speed profile for the ten
seconds before and after an event of each type. This will also create bounds
that will serve as the standard deviations from the average.

Those standard deviations will be used to determine which events are false
positives and need to be removed from the analysis.
"""

##############################################################################
# Section 1: Import packages and necessary files
##############################################################################


import pandas as pd
import statistics


event_type_list = ['fcw', 'ufcw', 'pcw', 'hwm', 'pdz']


# start looping through the different event types
for event_type_str in event_type_list:
    
    profile_name = event_type_str + '-speed-profile-percentage.pkl'
    datetime_file_name = event_type_str + '-event-datetimes.pkl'

    event_profile = pd.read_pickle(profile_name)
    event_datetimes = pd.read_pickle(datetime_file_name)


##############################################################################
# Section 2: Find the standard deviation of each column (second) of the speed
# profile
##############################################################################


    # these lists will hold the different standard deviations for the speed profile
    median_row = []
    std_dev_row = []
    double_std_dev_row = []
    
    
    # loop through each column to find standard deviation
    for x in range(len(event_profile.columns)):
        
        # load the column with the given index
        curr_column = event_profile.iloc[:, x]
        
        curr_median = round(statistics.median(curr_column), 0)
        curr_std_dev = round(statistics.stdev(curr_column), 1)
        dbl_std_dev = curr_std_dev * 2
        
        median_row.append(curr_median)
        std_dev_row.append(curr_std_dev)
        double_std_dev_row.append(dbl_std_dev)


##############################################################################
# Section 3: Build the 2 z-score profile
##############################################################################


    # now create a dataframe that outlines the average and the two standard
    # deviation bounds in each direction of the average
    btm_bound_2_list = []
    btm_bound_1_list = []
    up_bound_1_list = []
    up_bound_2_list = []
    
    for x in range(len(median_row)):
        btm_bound_2 = round(median_row[x] - double_std_dev_row[x], 1)
        btm_bound_1 = round(median_row[x] - std_dev_row[x], 1)
        up_bound_1 = round(median_row[x] + std_dev_row[x], 1)
        up_bound_2 = round(median_row[x] + double_std_dev_row[x], 1)
        
        btm_bound_2_list.append(btm_bound_2)
        btm_bound_1_list.append(btm_bound_1)
        up_bound_1_list.append(up_bound_1)
        up_bound_2_list.append(up_bound_2)
    
    range_of_values_dict = {
        'Bottom 2': btm_bound_2_list,
        'Bottom 1': btm_bound_1_list,
        "Median": median_row,
        "Upper 1": up_bound_1_list,
        "Upper 2": up_bound_2_list
        }
    
    range_of_values_df = pd.DataFrame(range_of_values_dict)


##############################################################################
# Section 4: Check to see which alerts are false positives
##############################################################################


    outlier_list = []
    
    
    # loop through the input file and determine which speed profiles indicate that
    # the event was a false positive
    for x in range(len(event_profile)):
        
        curr_profile = event_profile.iloc[x, :]
        
        curr_outlier_list = []
        
        # now loop through each of the speeds within the profile, and check to see
        # if any of the speeds exceed or fall below the second standard deviation
        # from the median
        for y in range(len(curr_profile)):
            
            curr_speed = curr_profile[y-10]
            up_bound_speed = range_of_values_df['Upper 2'][y]
            btm_bound_speed = range_of_values_df['Bottom 2'][y]
            
            curr_speed_above = curr_speed > up_bound_speed
            curr_speed_below = curr_speed < btm_bound_speed
            
            # if either the speed is above the upper bound or below the bottom
            # bound, then make the outlier variable equal to true
            if (curr_speed_above or curr_speed_below):
                outlier = True
                
            else:
                outlier = False
                
            curr_outlier_list.append(outlier)
            
        if any(curr_outlier_list):
            outlier_list.append(True)
        
        else:
            outlier_list.append(False)


##############################################################################
# Section 5: Add the outlier classification to the event datetime df. Export.
##############################################################################


    export_file_name = event_type_str + '-false-positives.pkl'

    event_datetimes['False Positive'] = outlier_list
    event_datetimes.to_pickle(export_file_name)


