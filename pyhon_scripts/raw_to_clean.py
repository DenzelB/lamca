#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 23 2020
Updated on Apr 15 2021

@author: darryloswald, denzelbrown
"""

##############################################################################
# Section 1: Import packages and necessary files
##############################################################################


import os
import pandas as pd

save_pickle_path = "./../raw_data"


##############################################################################
# Section 2: Set up the code to cycle through every pickle file in cwd
##############################################################################


# raw data files


# create a list of all of the pickle files that include data
files_list = os.listdir()

# create a list that only includes pickle files that store raw data
raw_data_files = []
for file in files_list:
    if file[-4:] == '.pkl':
        str_size = len(file)
        pickle_prefix_string = file[:str_size-4]
        raw_data_files.append(pickle_prefix_string)

raw_data_files = sorted(raw_data_files)


# cleaned data files


# create a list of all of the pickle files that include data
cleaned_folder_files_list = os.listdir("./../clean_data")

# create a list that only includes pickle files that store raw data
cleaned_data_files = []
for file in cleaned_folder_files_list:
    if file[-4:] == '.pkl':
        str_size = len(file)
        pickle_prefix_string = file[:str_size-4]
        
        if pickle_prefix_string[0] == 'c':
            pickle_data_string = pickle_prefix_string[8:]
            cleaned_data_files.append(pickle_data_string)

cleaned_data_files = sorted(cleaned_data_files)


# start the loop


for input_date in raw_data_files:
    
    if input_date in cleaned_data_files:
        pass
    else:

        input_file_string = input_date + '.pkl'
        output_file_string = save_pickle_path + 'cleaned-' + input_file_string
        input_df = pd.read_pickle(input_file_string)


##############################################################################
# Section 3: Create a new dataframe with only the columns we want
##############################################################################


        concise_df = pd.DataFrame()
        
        concise_df['Datetime'] = input_df['Loc Time']
        concise_df['Vehicle String'] = input_df['Vehicle Name']
        concise_df['Heading'] = input_df['Heading']
        concise_df['Daily Miles String'] = input_df['Distance In Miles']
        concise_df['Odometer String'] = input_df['Odometer']
        concise_df['Speed String'] = input_df['Speed']
        concise_df['Status'] = input_df['Status Name']
        concise_df['Rule Name'] = input_df['Rule Name']
        concise_df['Latitude String'] = input_df['Latitude']
        concise_df['Longitude String'] = input_df['Longitude']


##############################################################################
# Section 4: Change the data type of some columns so that they are easier to
# work with in other scripts
##############################################################################


        # Make sure that the vehicle name no longer has "LA Metro" in it
        vehicle_num_list = []
        for vehicle_string in concise_df['Vehicle String']:
            curr_vehicle_str = vehicle_string[-4:]
            curr_vehicle_num = int(curr_vehicle_str)
            vehicle_num_list.append(curr_vehicle_num)
        
        
        # Replace speeds that are NaN with 0
        speed_float_series = pd.to_numeric(concise_df['Speed String'], errors = 'coerce')
        corrected_speed_list = list(speed_float_series.fillna(0))
        
        
        # Make sure the daily miles and odometer columns are floats and not strings
        daily_miles_float_list = list(pd.to_numeric(concise_df['Daily Miles String'], errors = 'coerce'))
        odometer_float_list = list(pd.to_numeric(concise_df['Odometer String'], errors = 'coerce'))
        
        
        # Convert Lat/Long from strings into floats
        lat_float_list = list(pd.to_numeric(concise_df['Latitude String'], errors='coerce'))
        long_float_list = list(pd.to_numeric(concise_df['Longitude String'], errors='coerce'))


##############################################################################
# Section 5: Create the final dataframe
##############################################################################


        final_df = concise_df
        
        final_df = final_df.drop(columns=['Vehicle String', 'Speed String', 'Daily Miles String', 'Odometer String', 'Latitude String', 'Longitude String'])
        
        final_df['Vehicle'] = vehicle_num_list
        final_df['Speed'] = corrected_speed_list
        final_df['Daily Miles'] = daily_miles_float_list
        final_df['Odometer'] = odometer_float_list
        final_df['Latitude'] = lat_float_list
        final_df['Longitude'] = long_float_list


##############################################################################
# Section 6: Export the final dataframe to another pickle file
##############################################################################


        final_df.to_pickle(output_file_string)

